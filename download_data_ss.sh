#!/bin/bash
#

URL="http://187.191.75.115/gobmx/salud/datos_abiertos/datos_abiertos_covid19.zip"

mkdir -p ./data/zip
mkdir -p ./data/tmp

data_file_tmp="./data/tmp/datos_abiertos_covid19.zip"

if [ -f "$data_file_tmp" ]
then
    echo "INFO: something already downloaded, processing that."
else
    echo "INFO: Downloading ..."
    wget -O "$data_file_tmp" "$URL"
    echo "INFO: Done."    
fi

data_dir_tmp="./data/tmp/unzip"
mkdir -p "$data_dir_tmp"

if [ -d "$data_dir_tmp" ] && [ ! -z "$(ls $data_dir_tmp/*.csv 2> /dev/null)" ]
then
    echo "INFO: something already unzipped, processing that."
else
    echo "INFO: Unzipping ..."
    unzip -d "$data_dir_tmp" "$data_file_tmp"
    echo "INFO: Done."
fi

csv_file=$(ls "$data_dir_tmp"/*.csv )
csv_file=$(basename $csv_file)
if [ "$(echo "$csv_file" | wc -w)" -ne "1" ]
then
    echo "ERROR: After unnzip a single csv file must exist, and have no spaces in the name." >&2
    echo "Check here: ${csv_file}" >&2
    echo "See downloaded files in: ./data/tmp/" >&2
    exit 1
fi

# Read date from downloded file
year="20${csv_file:0:2}"
month="${csv_file:2:2}"
day="${csv_file:4:2}"
date_str="${year}-${month}-${day}"

cat <<EOF > data_pass.sh
export date_str=$date_str
echo date_str=\$date_str
EOF

echo "INFO: The downloaded data corresponds to $date_str"

# Deploy to a directory structure, using dates

data_file="./data/zip/datos_abiertos_covid19_${date_str}.zip"
if [ -f "$data_file" ]
then
    echo "WARNING: data file alreay here, overwriting." 
    echo "$data_file"
fi
mv "$data_file_tmp" "$data_file"
echo "INFO: zip file saved in $data_file"

data_dir="./data/${date_str}"
if [ -d "$data_dir" ] && [ ! -z "$(ls $data_dir/*.csv 2> /dev/null)" ]
then
    echo "WARNING: unzipped data alreay here, overwriting."
    echo "$data_dir"
fi
mkdir -p "$data_dir"
mv "$data_dir_tmp"/* "$data_dir/"
echo "INFO: unzipped data saved in $data_dir"

echo "INFO: Data succesfully downloaded and unzipped, for date:"
echo "$date_str"
