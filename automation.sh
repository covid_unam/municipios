# Automate data update

while true
do
    ./download_data_ss.sh
    source data_pass.sh
    python3 timeseries_municipios.py --date_str $date_str
    python3 make_graphs.py --date_str $date_str --level municipio
    # python3 t2.py --date_str $date_str
    git add .
    git commit -m "data update"
    git push
    sleep 24h
done
