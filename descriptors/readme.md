The files with names `Catalogos_0412_*` are data from the file `Catalogos_0412.xlsx`, available
[here](https://www.gob.mx/salud/documentos/datos-abiertos-152127). Currently, labels for MUNICIPIO are read from `mundb_2020_utf8.csv`.
