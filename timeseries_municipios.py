import pandas as pd
import argparse
import pathlib
import yaml
import glob
import numpy as np
from datetime import datetime as dt
from datetime import timedelta
from slugify import slugify

date_str           = None
date_fmt           = "%Y-%m-%d"
ent_mun_names_file = "./descriptors/mundb_2020_utf8.csv"

# DataFrame for municipio names
df_mun_names = None

def load_data () :

    print("INFO: reading data... ",
          end="", flush=True)

    global df_mun_names

    csv_file = glob.glob(
        "./data/{date_str}/*.csv".format(
            date_str=date_str))

    assert len(csv_file) > 0, "No csv file found."
    assert len(csv_file) < 2, "More than one csv file in the directory."

    try :
        df_data = pd.read_csv(
            csv_file[0])
    except UnicodeDecodeError :
        df_data = pd.read_csv(
            csv_file[0],
            encoding='latin-1')

    # Load names
    df_mun_names = pd.read_csv(
        ent_mun_names_file,
        dtype={'CVEGEO': object})
    df_mun_names.set_index("CVEGEO",inplace=True)

    print("done.")

    return df_data

def aggregate(df_data) :

    print("INFO: aggregate... ",
          end="", flush=True)

    pathlib.Path(
        "./data" +
        "/" + date_str +
        "/aggregate").mkdir(
        parents=True, exist_ok=True)

    data_agg_mun = {}
    data_agg_ent = {}

    # Aggregate data for MUNICIPIO_RES, and ENTIDAD_RES, using
    # FECHA_INGRESO
    fi_low_bound  = dt.strptime("2020-01-01", date_fmt)
    fi_high_bound = dt.strptime("2021-01-20", date_fmt)
    fi_min = fi_high_bound
    fi_max = fi_low_bound
    for i, row in df_data.iterrows() :

        try :
            ent_code = "{:02d}".format(int(row["ENTIDAD_RES"]))
        except :
            print("WARNING: unable to load ENTIDAD code for {}".format(row))
            ent_code = "Unknown"

        try :
            mun_code = "{:03d}".format(int(row["MUNICIPIO_RES"]))
        except ValueError :
            print("WARNING: unable to load MUNICIPIO code for {}".format(row))
            mun_code = "Unknown"

        # A single code for "ent" and "mun"
        em_code = ent_code + mun_code

        fi = dt.strptime(
            row["FECHA_INGRESO"],
            date_fmt)

        if fi < fi_low_bound or fi > fi_high_bound :
            print("WARNING: date out of bounds {}, data discarded.".format(fi))
            continue

        if fi < fi_min :
            fi_min = fi

        if fi > fi_max :
            fi_max = fi

        if em_code not in data_agg_mun :
            data_agg_mun[em_code] = {}

        if ent_code not in data_agg_ent :
            data_agg_ent[ent_code] = {}

        fi_str = fi.strftime(date_fmt)

        if fi_str not in data_agg_mun[em_code] :
            data_agg_mun[em_code][fi_str] = 0

        if row["RESULTADO"] == 1 :
            data_agg_mun[em_code][fi_str] += 1

        if fi_str not in data_agg_ent[ent_code] :
            data_agg_ent[ent_code][fi_str] = 0

        if row["RESULTADO"] == 1 :
            data_agg_ent[ent_code][fi_str] += 1

    print("done.")

    return data_agg_ent, data_agg_mun, fi_min, fi_max

def descriptor_mun (em_code) :
    try :
        em_name = df_mun_names["municipio"].loc[em_code]
        em_slug = ( str(em_code)[0:2]
                    + "_"
                    + slugify(em_name) )
        em_name_success = True
        found = True
    except KeyError :
        print("WARNING: {em_code} not in the name list.".format(
            em_code = em_code))
        em_name_success = False
        found = False
    except TypeError :
        print("WARNING: Some problem with {em_code}.".format(
            em_code = em_code))
        em_name_success = False
        found = None

    if not em_name_success :
        em_slug = em_code
        em_name = em_code

    return {
        "slug": em_slug,
        "name": em_name,
        "in_name_list": found,
        "entidad": str(em_code)[0:2],
        "municipio": str(em_code)[2:]}

def descriptor_ent (ent_code) :
    em_slug = ent_code
    em_name = ent_code

    return {
        "slug": em_slug,
        "name": em_name,
        "entidad": ent_code}

def export_descriptors (descriptors, level = "municipio") :

    file_path = "./data/{date_str}/aggregate/descriptors_{level}.yml".format(
        date_str = date_str,
        level    = level)

    with open( file_path, 'w') as f :
        yaml.dump(descriptors, f,
                  default_flow_style = False)

def generate_fi_list(fi_min, fi_max) :
    # TODO: check OBOE

    print("INFO: generating date list... ",
          end="", flush=True)

    days_min_max = (fi_max - fi_min).days

    date_list = [fi_min + timedelta(days=x)
            for x in range(days_min_max + 1)]

    print("done.")

    return date_list

def generate_table_new(data_agg, fi_list, level = "municipio") :

    print("INFO: generating table for new cases ({})... ".format(level),
          end="", flush=True)

    table = {}
    table["date"] = []
    descriptors = {}
    for key in data_agg :

        assert type(key) == str, "ERROR: key type."

        if level == "municipio" :
            descriptors[key] = descriptor_mun(key)
        elif level == "entidad" :
            descriptors[key] = descriptor_ent(key)

        table[key] = []

    for fi in fi_list :
        fi_str = fi.strftime(date_fmt)
        table["date"].append(fi_str)
        for key in data_agg :
            if fi_str in data_agg[key] :
                table[key].append(data_agg[key][fi_str])
            else :
                table[key].append(0)

    print("done.")

    return table, descriptors

def generate_table_cumsum(table_new) :

    print("INFO: generating table for cumulative cases... ",
          end="", flush=True)

    table_cumsum = {}
    table_cumsum["date"] = table_new["date"]

    for key in table_new :
        if key == "date" : continue
        table_cumsum[key] = np.cumsum(table_new[key])

    print("done.")

    return table_cumsum

def generate_table_names(table, descriptors) :

    print("INFO: generating table with names as column headers... ",
          end="", flush=True)

    table_names = {}
    table_names["date"] = table["date"]
    for key in descriptors :
        slug = descriptors[key]["slug"]
        table_names[slug] = table[key]

    print("done.")

    return table_names

def export_table(table,
                 level    = "municipio",
                 agg_type = "new",
                 sufix    = "codes" ) :

    filename = (
        "./data" +
        "/" + date_str +
        "/aggregate" +
        "/" + level + "_" + agg_type +
        "_" + sufix + ".csv")

    print("INFO: writing csv file {} ... ".format(filename),
          end="", flush=True)

    df_export = pd.DataFrame(table)
    df_export.set_index("date", inplace=True)

    df_export.to_csv(filename)

    print("done.")

def main():

    df = load_data()

    (data_agg_ent,
     data_agg_mun,
     fi_min,
     fi_max) = aggregate(df)

    fi_list = generate_fi_list(fi_min, fi_max)

    for data, level in [
            [data_agg_ent, "entidad"],
            [data_agg_mun, "municipio"]] :

        table, descriptors = generate_table_new(
            data,
            fi_list,
            level = level)

        export_table(
            table,
            level = level,
            agg_type = "new",
            sufix = "codes")

        if level == "municipio" :
            table_names = generate_table_names(
                table, descriptors)
            export_table(
                table_names,
                level = level,
                agg_type = "new",
                sufix = "names")

        export_descriptors(
            descriptors,
            level = level)

        table = generate_table_cumsum(table)

        export_table(
            table,
            level = level,
            agg_type = "cumulative",
            sufix = "codes")

        if level == "municipio" :
            table_names = generate_table_names(
                table, descriptors)
            export_table(
                table_names,
                level = level,
                agg_type = "cumulative",
                sufix = "names")

if __name__ == '__main__':

    def parse_arguments():
        parser = argparse.ArgumentParser(description='')
        parser.add_argument(
            '--date_str',
            help = "A string with the date in the format 2020-04-15",
            required = True)

        return parser.parse_args()

    p = parse_arguments()

    date_str = p.date_str

    main()
