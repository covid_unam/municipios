import pandas as pd
import argparse
import pathlib
import yaml
import sys
import glob
import numpy as np
from datetime import datetime as dt
from datetime import timedelta
from slugify import slugify
import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt

date_str           = None
date_fmt           = "%Y-%m-%d"
ent_mun_names_file = "./descriptors/mundb_2020_utf8.csv"

# DataFrame for municipio names
df_mun_names = None

# TODO: indented hierarchical messages

def load_names () :

    global df_mun_names

    print("INFO: reading names... ",
          end="", flush=True)

    # Load names
    df_mun_names = pd.read_csv(
        ent_mun_names_file,
        dtype={'CVEGEO': object})
    df_mun_names.set_index("CVEGEO",inplace=True)

    print("done.")

def load_data (date_str) :

    print("INFO: reading data for {date_str}... ".format(
        date_str=date_str),
          end="", flush=True)

    date_csv_dir = "./data/{date_str}/".format(date_str=date_str)

    csv_file = glob.glob(
        date_csv_dir + "*.csv")

    assert len(csv_file) > 0, "No csv file found in " + date_csv_dir
    assert len(csv_file) < 2, "More than one csv file in the directory " + date_csv_dir

    try :
        df_data = pd.read_csv(
            csv_file[0])
    except UnicodeDecodeError :
        df_data = pd.read_csv(
            csv_file[0],
            encoding='latin-1')

    assert "ID_REGISTRO" in df_data.keys(), "Column data not found: ID_REGISTRO"

    print("done.")

    return df_data

def check_diff(A, B, diff) :
    for a in diff :
        assert a in A, "set difference error"
        assert a not in B, "set difference error"

def get_diff(df_0, df_m1) :

    print("INFO: Processing difference... ",
          end="", flush=True)

    IDset_0  = set( df_0["ID_REGISTRO"])
    IDset_m1 = set(df_m1["ID_REGISTRO"])

    IDset_diff = IDset_0 - IDset_m1

    check_diff(
        IDset_0,
        IDset_m1,
        IDset_diff)

    mask = df_0["ID_REGISTRO"].isin(IDset_diff)

    df_diff = df_0[mask]

    assert len(IDset_diff) == len(df_diff), "Inconsistent data"

    print("done.")

    return df_diff

def export_diff(date_str, date_str_m1, df_diff) :

    filepath = "./data/{date_str}/diff/diff_{date_str}_{date_str_m1}.csv".format(
        date_str    = date_str,
        date_str_m1 = date_str_m1)

    print("INFO: writing csv file {}... ".format(filepath),
          end="", flush=True)

    df_diff.to_csv(filepath)

    print("done.")

def generate_fi_list(fi_min, fi_max) :
    # TODO: move this function to utils file

    print("INFO: generating date list... ",
          end="", flush=True)

    days_min_max = (fi_max - fi_min).days

    date_list = [fi_min + timedelta(days=x)
            for x in range(days_min_max + 1)]

    print("done.")

    return date_list

def plot(date_str, date_str_m1, df_diff) :

    filepath = "./graph/{date_str}/diff/diff_DEFUNCIONES_{date_str}_{date_str_m1}.png".format(
        date_str    = date_str,
        date_str_m1 = date_str_m1)

    print("INFO: creatig figure {}... ".format(filepath),
          end="", flush=True)

    # filter
    mask = df_diff["FECHA_DEF"] != "9999-99-99"

    # plot
    fig, ax =plt.subplots()

    df_filtered   = df_diff[mask]
    ds            = df_filtered["FECHA_DEF"]
    ds_cast       = ds.astype("datetime64")
    ds_aggregated = ds_cast.groupby(ds_cast.dt.day).count()

    print("")
    date_list = generate_fi_list(min(ds_cast), max(ds_cast))

    ydata = []
    for i, date_item in enumerate(date_list) :
        ip = i+1
        if ip in ds_aggregated.index :
            ydata.append(ds_aggregated[ip])
        else :
            ydata.append(0)

    ax.bar(date_list, ydata)

    ax.set_ylabel("Cantidad de defunciones")

    ax.set_title(
        "Defunciones por día, reportadas en {date_str}".format(
            date_str=date_str))

    ax.tick_params(axis='x', rotation=70)

    fig.tight_layout()

    fig.savefig(filepath)

    print("done.")

def main():

    date_0      = dt.strptime(date_str, date_fmt)
    date_m1     = date_0 - timedelta(days=1)
    date_str_m1 = date_m1.strftime(date_fmt)

    df_0  = load_data(date_str)
    df_m1 = load_data(date_str_m1)

    load_names()

    pathlib.Path(
        "./data" +
        "/" + date_str +
        "/diff").mkdir(
        parents=True, exist_ok=True)

    df_diff = get_diff(df_0, df_m1)

    export_diff(date_str, date_str_m1, df_diff)

    pathlib.Path(
        "./graph" +
        "/" + date_str +
        "/diff").mkdir(
        parents=True, exist_ok=True)

    plot(date_str, date_str_m1, df_diff)

if __name__ == '__main__':

    def parse_arguments():
        parser = argparse.ArgumentParser(description='')
        parser.add_argument(
            '--date_str',
            help = "A string with the date in the format 2020-04-15",
            required = True)

        return parser.parse_args()

    p = parse_arguments()

    date_str = p.date_str

    main()
