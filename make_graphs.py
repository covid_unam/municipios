import sys
import pathlib
import argparse
import pandas as pd
import numpy as np
from datetime import datetime as dt
import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt
from matplotlib import animation

date_str = "2020-04-15"
date_fmt = "%Y-%m-%d"
level = "municipio"

def main():

    print("INFO: reading data... ",
          end="", flush=True)

    data_file = (
        "./data" +
        "/" + date_str +
        "/aggregate" +
        "/municipio_cumulative_names.csv" )

    df = pd.read_csv(data_file, index_col="date")

    print("done.")

    print("INFO: looking for the top 10... ",
          end="", flush=True)

    dft = df.transpose()

    dft.index.rename("mun", inplace=True)

    date_list = [
        dt.strptime(item, date_fmt)
        for item
        in dft.keys() ]

    date_max = max(date_list)
    date_max_str = date_max.strftime(date_fmt)

    top10_df = dft.sort_values(
        ascending = False,
        by = date_max_str).head(10)

    y_max = max(top10_df[date_max_str])

    top10 = list(top10_df.index)

    print("done.")

    print("INFO: creating output directory... ",
          end="", flush=True)

    graph_dir = (
        "./graph" +
        "/" + date_str)

    pathlib.Path(graph_dir).mkdir(
        parents=True, exist_ok=True)

    print("done.")

    print("INFO: generating figures... ",
          flush=True)

    x = date_list

    fig, ax = plt.subplots()

    line_list = []
    for col in top10 :
        line_list.append(ax.plot(0, label=col, marker='.')[0])

    ax.set_xlim(min(x), max(x))
    ax.set_ylim(0, y_max)

    ax.set_title("Municipios con mayor cantidad de casos confirmados")

    ax.set_ylabel("Casos confirmados")
    ax.set_xlabel("Fecha")

    plt.xticks(rotation=30)

    ax.legend()

    frames = range(len(x))

    def animate (i) :
        print("--- figure {} / {}".format(i,len(x)),
              flush=True)
        for j, col in enumerate(top10) :
            y = df[col]
            line_list[j].set_data(x[:i], y[:i])
        if i == 0 :
            plt.tight_layout()

        return line_list

    anim = animation.FuncAnimation(
        fig,
        animate,
        frames = frames,
        blit=True)

    anim_filename = graph_dir + "/" + level + ".mp4"

    anim.save(anim_filename)

    print("--- done.")

    print("INFO: animation saved in\n" + anim_filename)

if __name__ == '__main__':

    def parse_arguments():
        parser = argparse.ArgumentParser(description='')
        parser.add_argument(
            '--date_str',
            help = "A string with the date in the format 2020-04-15",
            required = True)
        parser.add_argument(
            '--level',
            choices = ["municipio", "entidad"],
            required = True)

        return parser.parse_args()

    p = parse_arguments()

    date_str = p.date_str
    level    = p.level

    main()
