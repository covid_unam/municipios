# Dependencies

The virtual machine for this code, described [here](./mybinder.md), is built with dependencies listed in these files:
- [./environment.yml](./environment.yml)
- [./install.R](./install.R)
- [./runtime.txt](./runtime.txt)

For convenience, libraries are also listed here.

## Python 3.7

- pandas
- argparse
- yaml
- pathlib
- glob
- numpy
- datetime
- slugify
- matplotlib

## Bash

- wget
- unzip
- ffmpeg

## R

- tidyverse
- rmarkdown
- httr
- shinydashboard
- leaflet
- stringr
- rgdal
- spdplyr
- geojsonio
- rmapshaper
- sf
- zip
- dplyr
- glue
- ggplot2
- gganimate
- tidyverse
- janitor
