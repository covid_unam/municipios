Confirmed cases.

# Aggragated with `MUNICIPIO_RES`

The field `MUNICIPIO_RES` corresponds to residency "MUNICIPIO".

Data with values unable to be read, are labeled as `Unknown`.

Headers in the `_codes` files correspond to [this
file](../mundb_2020_utf8.csv).

Headers in the `_names` files have a number because there are multiple
MUNICIPIO with the same name, in different ENTIDAD. To see how this header (slug) is related to other data see the file `descriptors.yml`.

Column order is not the same in `_names` and `_codes` files.

# Aggreagated with `ENTIDAD_RES`

The field `ENTIDAD_RES` corresponds to residency "ENTIDAD".

Data with values unable to be read, are labeled as `Unknown`.



