# MyBinder

If you want to play with this data and this code, you can use a free virtual machine provided by https://mybinder.org/

In principle, a virtual machine is built with the latest version of
this repo. However, this process can take up to 30 min. Then, it is a
good idea to use a prebuilt machine, which loads in a minute. Once
there, you can simply type

~~~bash
git pull
bash download_data_ss.sh
python3 timeseries_municipios.py --date_str 2020-04-22
python3 make_graphs.py --date_str 2020-04-22 --level municipio
~~~

(using the apropriate value for date_str), and get the latest version of the code, and the results on the latest data.

Additionally, you can use the same prebuilt machine with some different interfaces: jupyter, jupyterLab, Rstudio, shiny (not working).

## Prebuilt machines

- 2020-04-22,
[jupyter](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.1?urlpath=tree), [jupyterLab](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.1?urlpath=lab), [Rstudio](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.1?urlpath=rstudio),

- 2020-04-24,
[jupyter](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.2?urlpath=tree), [jupyterLab](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.2?urlpath=lab), [Rstudio](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.2?urlpath=rstudio),

- 2020-05-11,
[jupyter](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.3?urlpath=tree), [jupyterLab](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.3?urlpath=lab), [Rstudio](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/jupyter-0.3?urlpath=rstudio),

## Build a new machine

https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.com%2Fcovid_unam%2Fmunicipios.git/master
