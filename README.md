# Municipios

COVID-19 cases timeseries for "municipios" in México.

In this repository, you can find aggregate data (based on data available [here](https://www.gob.mx/salud/documentos/datos-abiertos-152127)), and the correspondig scripts.

## Dependencies

See [here](./dependencies.md).

## Usage

```bash
bash download_data_ss.sh
python3 timeseries_municipios.py --date_str 2020-04-22
```
where `date_str` is a date value, as printed at the end of a successful
execution of `bash download_data_ss.sh`, which is typically not the
current date, but the day before.

If you want to play online with this code, and data, see [here](./mybinder.md).

## Downloaded data

To avoid duplicating data, temporary data is not currently in this
repository, only final data files.
