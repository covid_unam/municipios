import pandas as pd
import argparse
import pathlib
import yaml
import sys
import glob
import numpy as np
from datetime import datetime as dt
from datetime import timedelta
from slugify import slugify
import matplotlib
matplotlib.use("agg")
from matplotlib import pyplot as plt

date_str           = None
date_fmt           = "%Y-%m-%d"

def load_data (date_str) :

    print("INFO: reading data for {date_str}... ".format(
        date_str=date_str),
          end="", flush=True)

    date_csv_dir = "./data/{date_str}/".format(date_str=date_str)

    csv_file = glob.glob(
        date_csv_dir + "*.csv")

    assert len(csv_file) > 0, "No csv file found in " + date_csv_dir
    assert len(csv_file) < 2, "More than one csv file in the directory " + date_csv_dir

    try :
        df_data = pd.read_csv(
            csv_file[0])
    except UnicodeDecodeError :
        df_data = pd.read_csv(
            csv_file[0],
            encoding='latin-1')

    assert "ID_REGISTRO" in df_data.keys(), "Column data not found: ID_REGISTRO"

    print("done.")

    return df_data

def check_diff(A, B, diff) :
    for a in diff :
        assert a in A, "set difference error"
        assert a not in B, "set difference error"

def get_diff(df_0, df_m1) :

    print("INFO: Processing difference... ",
          end="", flush=True)

    IDset_0  = set( df_0["ID_REGISTRO"])
    IDset_m1 = set(df_m1["ID_REGISTRO"])

    IDset_diff = IDset_0 - IDset_m1

    check_diff(
        IDset_0,
        IDset_m1,
        IDset_diff)

    mask = df_0["ID_REGISTRO"].isin(IDset_diff)

    df_diff = df_0[mask]

    assert len(IDset_diff) == len(df_diff), "Inconsistent data"

    print("done.")

    return df_diff

def generate_fi_list(fi_min, fi_max) :
    # TODO: move this function to utils file

    print("INFO: generating date list... ",
          end="", flush=True)

    days_min_max = (fi_max - fi_min).days

    date_list = [fi_min + timedelta(days=x)
            for x in range(days_min_max + 1)]

    print("done.")

    return date_list

def plot_1(date_str, date_str_m1, ds_aggregated, ds_cast, mean, std) :

    filepath = "./data/t3/diff_DEFUNCIONES_{date_str}_{date_str_m1}_mean_std.png".format(
        date_str    = date_str,
        date_str_m1 = date_str_m1)

    print("INFO: creatig figure {}... ".format(filepath),
          end="", flush=True)

    # plot
    fig, ax = plt.subplots()

    print("")
    date_list = generate_fi_list(min(ds_cast), max(ds_cast))

    ydata = []
    for date_item in date_list :
        if date_item in ds_aggregated.index :
            ydata.append(ds_aggregated[date_item])
        else :
            ydata.append(0)

    ax.bar(date_list, ydata)

    # TODO: check OBOE
    ax.axvline(max(ds_cast) - timedelta(days=mean),     color='k')
    ax.axvline(max(ds_cast) - timedelta(days=mean-std), color='r')
    ax.axvline(max(ds_cast) - timedelta(days=mean+std), color='r')

    ax.set_ylabel("Cantidad de defunciones")

    ax.set_title(
        "Defunciones por día, reportadas en {date_str}".format(
            date_str=date_str))

    ax.tick_params(axis='x', rotation=70)

    fig.tight_layout()

    fig.savefig(filepath)

    plt.close(fig)
    
    print("done.")

def plot_aggregate(data_dic) :

    filepath = "./data/t3/diff_aggregate.png"

    print("INFO: creatig figure {}... ".format(filepath),
          end="", flush=True)

    fig, ax = plt.subplots()

    xdata = [ dt.strptime(item["date_str"], date_fmt)
              for item
              in data_dic ]

    ydata = [ item["mean"]
              for item
              in data_dic ]

    edata = [ item["std"]
              for item
              in data_dic ]

    ax.errorbar(xdata, ydata, edata,
                linestyle='None', marker='o')

    ax.tick_params(axis='x', rotation=70)

    ax.set_title("Días transcurridos entre el deceso y su registro")

    ax.set_ylim(bottom=0)

    fig.tight_layout()

    fig.savefig(filepath)

    plt.close(fig)

    print("done.")

def main():

    date_0 = dt.strptime(date_str, date_fmt)

    data_dic = []
    i = 0
    while i is not None :

        date_i     = date_0 - timedelta(days=i)
        date_str_i = date_i.strftime(date_fmt)

        pathlib.Path(
            "./data/t3").mkdir(
            parents=True, exist_ok=True)

        try :
            df_i = load_data(date_str_i)
        except Exception as e:
            print(e)
            print("INFO: Data processed up to {}".format(date_str_ip1))
            break

        if i != 0 :
            df_diff = get_diff(df_ip1, df_i)

            # if date_str_ip1 == "2020-05-13" :
            #     breakpoint()

            # filter
            mask = df_diff["FECHA_DEF"] != "9999-99-99"

            df_filtered   = df_diff[mask]
            ds            = df_filtered["FECHA_DEF"]
            ds_cast       = ds.astype("datetime64")
            ds_aggregated = ds_cast.groupby(ds_cast).count()

            agg_days = [ (date_ip1 - item).days
                         for item
                         in ds_aggregated.index ]

            mean    = np.mean(agg_days)
            std     = np.std(agg_days)

            plot_1(date_str_ip1, date_str_i, ds_aggregated, ds_cast, mean, std)

            data_dic.append({
                "date_str"    : date_str_i,
                "mean"        : mean,
                "std"         : std })

        df_ip1       = df_i
        date_str_ip1 = date_str_i
        date_ip1     = date_i
        i           += 1

    plot_aggregate(data_dic)

if __name__ == '__main__':

    def parse_arguments():
        parser = argparse.ArgumentParser(description='')
        parser.add_argument(
            '--date_str',
            help = "A string with the date in the format 2020-04-15",
            required = True)

        return parser.parse_args()

    p = parse_arguments()

    date_str = p.date_str

    main()
