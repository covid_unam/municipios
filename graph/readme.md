These visualizations are not quite ready.

After corresponding aggreagated data is created, these visualizations
 are built with the command
```bash
python3 make_graphs.py --date_str 2020-04-22 --level municipio
```
where `date_str` is a date value, as the directory names in `data` directory, which is typically not the
current date, but the previous days.

By now, they only have 10 values of "MUNICIPIO", with the highest
incidence.

Inside jupyterLab these mp4 files can be viewed using the option "open in new browser tab".
